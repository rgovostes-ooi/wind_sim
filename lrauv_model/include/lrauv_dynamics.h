#ifndef PROJECT_LRAUV_DYNAMICS_H
#define PROJECT_LRAUV_DYNAMICS_H


void dynamics(double& ut, double& vt, double& wt, double& pt, double& qt, double& rt, // body-frame accelerations
	      const double u, const double v, const double w,  // current body-frame velocities
	      const double r, const double p, const double q,  // current body-frame rates
	      const double phi, const double theta, const double psi, // current roll, pitch, yaw
	      const double rpm, const double rudder_deg, const double sternplane_deg,  // inputs
	      const double U, const double V); // currents, setting to the north and east.

#endif //PROJECT_LRAUV_DYNAMICS_H
