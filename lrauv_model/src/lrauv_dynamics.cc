
#include <math.h>

#include <ds_libtrackline/Trackline.h>

#include <lrauv_dynamics.h>

// Constants for quasi-kinematic model
#define TAU 1.0 // (s) Time constant for vehicle responsiveness.
#define RPM2SPD 0.01 // 100 rpm per m/s
#define RUD2YAW 0.3 // deg/s per deg of rudder deflection at 1 m/s
#define STRN2PTCH 0.3 // deg pitch per deg of stern plane deflection at 1 m/s


/*
  
  Compute body-frame acceleration
  given current velocity, attitude (roll and pitch only, for buoyancy terms) and control
  inputs.  Control inputs are thruster rpm and fin angles (rudder 
  and stern plane) in degrees.
  
  Notation is that of Abkowitz 1972 (hydrodynamic derivatives) and Fossen 1994.

  Handling of currents is based on Hegrenaes 2011.

  Quasi-kinematic model:
  * Body-frame forward speed generates acceleration toward a rpm-dependent speed
  * Fins generate rotational acceleration toward an angle-dependent turn rate and pitch angle.

*/

void dynamics(double& ut, double& vt, double& wt, double& pt, double& qt, double& rt, // body-frame accelerations
	      const double u, const double v, const double w,  // current body-frame velocities
	      const double p, const double q, const double r,  // current body-frame rates
	      const double phi, const double theta, const double psi, // current roll, pitch, yaw
	      const double rpm, const double rudder, const double sternplane,  // inputs
	      const double U, const double V) // currents, setting to the north and east.
{

  // coordinate convention for fin angles is from Prestero 2001.
  // dr > 0 means fin is pointing to stbd, causing a turn to port.
  // ds > 0 means stern planes are pointing up causing vehicle to pitch down.
  double dr = rudder;
  double ds = sternplane;

  // Handle currents under the assumption they are irrotational and slowly varying.  Then
  // accelerations in the inertial frame become nearly equal to accelerations in the current-relative
  // frame.
  //vrot... @@@@@@  2d version for now.
  double ur = u - (cos(psi)*U + sin(psi)*V);  
  double vr = v - (-sin(psi)*U + cos(psi)*V);
  double wr = w;
  
  
  // Compute accelerations for simulated DOF.
  rt = 1/TAU*(RUD2YAW*ur*fabs(ur)*(-dr) - r);
  qt = 1/TAU/TAU*(STRN2PTCH*ur*fabs(ur)*(-ds) - theta - q*10);  // damping coefficient determined ad hoc
  ut = 1/TAU*(RPM2SPD*rpm - ur*fabs(ur));

  // Integration is done externally.  Nonlinearity and numerical noise will cause phi,v and perhaps other
  // unsimulated dynamics to drift, corrupting the assumptions made here.  So add some bogus dynamics to
  // regulate these unmodeled dynamics to 0.
  // This scheme ends up being slightly underdamped in p/phi
  vt = -1/TAU*vr*10.0;
  wt = -1/TAU*wr;
  // phi is more complicated because of the structure of the euler rate matrix.  For non-zero r and theta, a
  // non-zero p is necessary to regulate phi to 0 (and phit to 0).
  double phitd = -1/TAU*phi; // desired phi dynamics
  double pd = phitd - (q*sin(phi)*tan(theta) + r*cos(phi)*tan(theta)); // compensation for non-linear kinematics
  pt = 1/TAU*(pd-p);

}

/*
  line-follow
  dl = oline(START(1),START(2),GOAL(1),GOAL(2),n,e);
  psil = atan2(GOAL(2)-START(2),GOAL(1)-START(1));
  psid = psil + sat(-0.1*dl,60*D2R);
  if LF
    dr = sat(-1*R2D*(psid - psi),25);
  end

 */

#define fsat(x,flim) (((x)>(flim))? (flim) : (((x)<(-flim))? (-flim): (x)))

void line_follow(const ds_trackline::Trackline& trackline, const double& n, const double& e, const double& heading_deg, double& rudder_deg) {

  using namespace ds_trackline;
  
  Trackline::VectorEN along_across = trackline.en_to_trackframe(e,n);

  double dl = along_across[0];
  double psil = trackline.getCourseRad();
  double psid = psil + fsat(-0.1*dl,60*M_PI/180.0);
  double dr = fsat(-100.0*(psid - heading_deg*M_PI/180.0),25*M_PI/180.0);

  rudder_deg = dr*180.0/M_PI;
  
}

void bottom_follow(const double& altd, const double& alt, const double& slope_deg, const double& heading_deg, double& sternplane_deg ) {

  // regulate pitch to match slope at desired altitude.
  // slope is positive when going uphill.

  double theta = heading_deg*M_PI/180.0;
  double thetad = (slope_deg*M_PI/180.0+10*M_PI/180.0*(altd-alt));
  sternplane_deg = fsat(-500.0*(thetad - theta),25);

  
}



