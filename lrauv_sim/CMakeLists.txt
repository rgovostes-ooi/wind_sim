cmake_minimum_required(VERSION 2.8.3)
project(lrauv_sim)

add_compile_options(-std=c++11)

find_package(GDAL REQUIRED)

list(INSERT CMAKE_MODULE_PATH 0 ${PROJECT_SOURCE_DIR}/cmake)

find_package(catkin REQUIRED COMPONENTS
   ds_sim
   roscpp
   gazebo_ros
   tf
   lrauv_model
)

find_package(gazebo REQUIRED)

link_directories(${GAZEBO_LIBRARY_DIRS} /usr/local/lib/)
include_directories(${Boost_INCLUDE_DIR} ${catkin_INCLUDE_DIRS} ${GAZEBO_INCLUDE_DIRS} ${ds_sim_INCLUDE_DIRS} /usr/local/include/ocean_models)


add_library(lrauvros_dynamics src/lrauv_dynamics_plugin.cc)
target_link_libraries(lrauvros_dynamics
  ${catkin_LIBRARIES}
  ${GAZEBO_LIBRARIES} 
  ${GDAL_LIBRARY}
  ocean_models
  )
add_dependencies(lrauvros_dynamics ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

catkin_package(
LIBRARIES lrauvros_dynamics
CATKIN_DEPENDS ds_sim roscpp gazebo_ros tf lrauv_model
)
