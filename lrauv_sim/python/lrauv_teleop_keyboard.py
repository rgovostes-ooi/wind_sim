#!/usr/bin/env python
import rospy

from std_msgs.msg import Float32

import sys


import tty, termios

# got this online.  pip2 install getch failed.
def getch():
    """getch() -> key character
    
    Read a single keypress from stdin and return the resulting character. 
    Nothing is echoed to the console. This call will block if a keypress 
    is not already available, but will not wait for Enter to be pressed. 
    
    If the pressed key was a modifier key, nothing will be detected; if
    it were a special function key, it may return the first character of
    of an escape sequence, leaving additional characters in the buffer.
    """
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(fd)
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch
    
RUDDER_TOPIC = '/lrauv/rudder_cmd_deg'
STERNPLANE_TOPIC = '/lrauv/sternplane_cmd_deg'
PROPELLER_TOPIC = '/lrauv/propeller_cmd_rpm'

LEFT = '\x1b'+'[D'
RIGHT = '\x1b'+'[C'
UP = '\x1b'+'[A'
DN = '\x1b'+'[B'
    
def talker():

    pubRudder = rospy.Publisher(RUDDER_TOPIC, Float32, queue_size=1)
    pubSternplane = rospy.Publisher(STERNPLANE_TOPIC, Float32, queue_size=1)
    pubPropeller = rospy.Publisher(PROPELLER_TOPIC, Float32, queue_size=1)
    
    rospy.init_node('lrauv_teleop_keyboard', anonymous=True)

    dr = 0
    ds = 0
    rpm = 0
    
    while not rospy.is_shutdown():

        # see doco above.  nesting is for dealing with
        # arrow keys.
        key=getch()
        if key == '\x1b':
            key1 = getch()
            if key1 == '[':
                key2 = getch()
                key = key + key1 + key2

        # interpret key presses.
        if key == str(LEFT): # turns toward port
            dr = dr+1;
            print('rudder: ' + str(dr))
        elif key == RIGHT: # turns toward stbd
            dr = dr-1;
            print('rudder: ' + str(dr))
        elif key == UP: # dives
            ds = ds+1;
            print('sternplane: ' + str(ds))
        elif key == DN: # climbs
            ds = ds-1;
            print('sternplane: ' + str(ds))
        elif key == 'q': # speed up. 
            rpm = rpm+10;
            print('rpm: ' + str(rpm))
        elif key == 'a': # slow down
            rpm = rpm-10;
            print('rpm: ' + str(rpm))
        elif key == '0': # zero fin angles
            ds = 0
            dr = 0
            print('zero fin angles.')
        else:
            print('Unknown key.' + key)


        pubRudder.publish(dr)
        pubSternplane.publish(ds)
        pubPropeller.publish(rpm)

            
"""
      elseif key == 's' % decrease current setting to north
      U = U-0.1;
    elseif key == 'n' % increase current setting to north
      U = U+0.1;
    elseif key == 'w' % decrease current setting to east
      V = V-0.1;
    elseif key == 'e' % increase current setting to east
      V = V+0.1;
    elseif key == 'u' % decrease down-welling
      W = W-0.1;
    elseif key == 'd' % increase down-welling
      W = W+0.1;
    elseif key == 'b' % engage bottom-follower
      BF = true;
    elseif key == 'l' % engage line-follower
      LF = true;
    elseif key == 'x' % disengage bottom-follower and line-follower
      BF = false;
      LF = false;
    end
"""



 
if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
