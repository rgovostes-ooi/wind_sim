
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>

#include <thread>
#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <ros/subscribe_options.h>
#include "std_msgs/Float32.h"

#include <ds_sim/common.hpp>

#include <geometry_msgs/Wrench.h>
#include <geometry_msgs/Accel.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/WrenchStamped.h>

#include <boost/bind.hpp>
#include <tf/transform_datatypes.h>
#include <sensor_msgs/JointState.h>
#include <algorithm>

// Functionality from GDAL for projections
#include <gdal/ogr_spatialref.h>

// ocean_models for currents.
#include <model_interface/ModelInterface.h>
#include <model_interface/ModelData.h>
#include <general_models/ConstantModel.h>
#include <fvcom/FVCOM.h>

#include <lrauv_dynamics.h>

bool loadString(std::string &result, const sdf::ElementPtr& _sdf, const std::string& tag) {
    if (!_sdf->HasElement(tag)) {
        ROS_FATAL_STREAM("ROS plugin lrauvdynamics requires a tag \"" <<tag <<"\" but it wasn't found!");
        return false;
    }

    result = _sdf->Get<std::string>(tag);

    // remove any double-quotes
    result.erase(std::remove(result.begin(), result.end(), '\"'),result.end());

    ROS_INFO_STREAM("ROS plugin lrauvdynamics load \"" <<tag <<"\" = \"" <<result <<"\"");

    return true;
}

bool loadDouble(double& result, const sdf::ElementPtr& _sdf, const std::string& tag) {
    if (!_sdf->HasElement(tag)) {
        ROS_FATAL_STREAM("ROS plugin lrauvdynamics requires a tag \"" <<tag <<"\" but it wasn't found!");
        return false;
    }

    result = _sdf->Get<double>(tag);

    ROS_INFO_STREAM("ROS plugin lrauvdynamics load \"" <<tag <<"\" = \"" <<result <<"\"");

    return true;
}

bool loadInt(int& result, const sdf::ElementPtr& _sdf, const std::string& tag) {
    if (!_sdf->HasElement(tag)) {
        ROS_FATAL_STREAM("ROS plugin lrauvdynamics requires a tag \"" <<tag <<"\" but it wasn't found!");
        return false;
    }

    result = _sdf->Get<int>(tag);

    ROS_INFO_STREAM("ROS plugin lrauvdynamics/servo load \"" <<tag <<"\" = \"" <<result <<"\"");

    return true;
}


namespace gazebo {
class LrauvDynamics : public ModelPlugin {
  public:
    LrauvDynamics() {}

    void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf) {

      //
      this->model = _parent;
      this->body_link = _parent->GetLink();
      this->sternplane_joint = _parent->GetJoint("horizontal_servo");
      this->rudder_joint = _parent->GetJoint("vertical_servo");
      this->propeller_joint = _parent->GetJoint("main_propeller_hub");
      
      // Moving links in gazebo requires dynamics.  Setting position or velocity
      // presumably creates infinite accelerations which cause problems.
      this->rudder_pid = common::PID(1.0, 0, 0.0);
      this->model->GetJointController()->SetPositionPID(
          this->rudder_joint->GetScopedName(), this->rudder_pid);        // Apply the P-controller to the joint.
      this->sternplane_pid = common::PID(1.0, 0, 0.0);
      this->model->GetJointController()->SetPositionPID(
          this->sternplane_joint->GetScopedName(), this->sternplane_pid);  // Apply the P-controller to the joint.


      // propeller
      this->propeller_pid = common::PID(1.0, 0, 0.0);
      this->model->GetJointController()->SetVelocityPID(
          this->propeller_joint->GetScopedName(), this->propeller_pid);  // Apply the P-controller to the joint.

      
      // Listen to the update event
      this->updateConnection = event::Events::ConnectWorldUpdateBegin(
          boost::bind(&LrauvDynamics::OnUpdate, this, _1));


      // Initialize ros, if it has not already bee initialized.
      if (!ros::isInitialized())
	{
	  int argc = 0;
	  char **argv = NULL;
	  ros::init(argc, argv, "gazebo_client",
		    ros::init_options::NoSigintHandler);
	}

      // Create our ROS node. This acts in a similar manner to
      // the Gazebo node
      this->rosNode.reset(new ros::NodeHandle("gazebo_client")); // what does this name do?  no new node shows up, instead /gazebo has the subscription below.
      
      // Subscriptions
      ros::SubscribeOptions so1 =
	ros::SubscribeOptions::create<std_msgs::Float32>(
							 "/" + this->model->GetName() + "/rudder_cmd_deg",
							 1,
							 boost::bind(&LrauvDynamics::OnRudderCmdMsg, this, _1),
							 ros::VoidPtr(), &this->rosQueue);
      this->subRudderCmd = this->rosNode->subscribe(so1);
      ros::SubscribeOptions so2 =
	ros::SubscribeOptions::create<std_msgs::Float32>(
							 "/" + this->model->GetName() + "/sternplane_cmd_deg",
							 1,
							 boost::bind(&LrauvDynamics::OnSternplaneCmdMsg, this, _1),
							 ros::VoidPtr(), &this->rosQueue);
      this->subSternplaneCmd = this->rosNode->subscribe(so2);
      ros::SubscribeOptions so3 =
	ros::SubscribeOptions::create<std_msgs::Float32>(
							 "/" + this->model->GetName() + "/propeller_cmd_rpm",
							 1,
							 boost::bind(&LrauvDynamics::OnPropellerCmdMsg, this, _1),
							 ros::VoidPtr(), &this->rosQueue);
      this->subPropellerCmd = this->rosNode->subscribe(so3);

      
      
      // Spin up the queue helper thread.
      this->rosQueueThread =
	std::thread(std::bind(&LrauvDynamics::QueueThread, this));
      
      
    }

    void OnUpdate(const common::UpdateInfo& _info) {

      // Do nothing.  Hydrodynamic forces computed in a world plugin.
      
    }


    void initState(const common::UpdateInfo& _info) {

      // Is this needed if using gazebo for dynamics instead of just
      // for display?  Shouldn't this be in one of the initialization files?
      //body_link->SetWorldPose(...);
      
    }


  /// \brief Set the sternplane angle
  /// \param[in] _ds_deg sternplane angle in deg
  void SetSternplaneAngleDeg(const double &_ds_deg)
  {
    this->model->GetJointController()
      ->SetPositionTarget(this->sternplane_joint->GetScopedName(), _ds_deg*M_PI/180.0);
  }

  /// \brief Set the rudder angle
  /// \param[in] _dr_deg rudder angle in deg
  void SetRudderAngleDeg(const double &_dr_deg)
  {
    this->model->GetJointController()
      ->SetPositionTarget(this->rudder_joint->GetScopedName(), _dr_deg*M_PI/180.0);
  }

  /// \brief Set the propeller rpm.
  /// \param[in] _prop_rpm propeller speed in rpm
  void SetPropellerRpm(const double &_prop_rpm)
  {
    // no joint or visualization for now; just a class member
    this->prop_rpm = _prop_rpm;
    // @@@ delete above

    std::cout << "Got prop rpm: " <<_prop_rpm << " radps: " << _prop_rpm*2.0*M_PI/60.0 << std::endl;
    
    this->model->GetJointController()
      ->SetVelocityTarget(this->propeller_joint->GetScopedName(), _prop_rpm*2.0*M_PI/60.0);
    
  }

  
  protected:

    gazebo::physics::ModelPtr model;
    gazebo::physics::LinkPtr body_link;
    gazebo::physics::JointPtr sternplane_joint;
    gazebo::common::PID sternplane_pid;
    gazebo::physics::JointPtr rudder_joint;
    gazebo::common::PID rudder_pid;
    gazebo::physics::JointPtr propeller_joint;
    gazebo::common::PID propeller_pid;

  gazebo::event::ConnectionPtr updateConnection;

    ocean_models::FVCOM* ocean_model;
  
  double prop_rpm = 0.0;

private:
    void OnRudderCmdMsg(const std_msgs::Float32ConstPtr &_msg)
    {
      this->SetRudderAngleDeg(_msg->data);
    }
  void OnSternplaneCmdMsg(const std_msgs::Float32ConstPtr &_msg)
    {
      this->SetSternplaneAngleDeg(_msg->data);
    }
    void OnPropellerCmdMsg(const std_msgs::Float32ConstPtr &_msg)
    {
      this->SetPropellerRpm(_msg->data);
    }  


/// \brief ROS helper function that processes messages
private: void QueueThread()
{
  static const double timeout = 0.01;
  while (this->rosNode->ok())
  {
    this->rosQueue.callAvailable(ros::WallDuration(timeout));
  }
}

  /// \brief A node use for ROS transport
private: std::unique_ptr<ros::NodeHandle> rosNode;

/// \brief A ROS subscriber
private: ros::Subscriber subRudderCmd;
private: ros::Subscriber subSternplaneCmd;
private: ros::Subscriber subPropellerCmd;


/// \brief A ROS callbackqueue that helps process messages
private: ros::CallbackQueue rosQueue;

/// \brief A thread the keeps running the rosQueue
private: std::thread rosQueueThread;
  
};

GZ_REGISTER_MODEL_PLUGIN(LrauvDynamics)
};

