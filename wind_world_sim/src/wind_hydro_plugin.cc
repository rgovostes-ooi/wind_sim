#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"

// Functionality from GDAL for projections
#include <gdal/ogr_spatialref.h>

// ocean_models for currents.
#include <model_interface/ModelInterface.h>
#include <model_interface/ModelData.h>
#include <general_models/ConstantModel.h>
#include <fvcom/FVCOM.h>

enum ocean_currents_model_t
  {
    CONSTANT,
    FVCOM
  };


struct ocean_currents_t
{
  // model type
  ocean_currents_model_t model;

  // constant current parameters
  double constant_east;
  double constant_north;
  
  // fvcom parameters
  int fvcom_epsg;
  std::string fvcom_uri;
  OGRSpatialReference fvcom_srs;
  OGRCoordinateTransformation *fvcom_poCT;
  
};


struct ocean_effects_t
{
  ocean_currents_t currents;
};


namespace gazebo
{
class WindHydroPlugin : public WorldPlugin
{
  public: void Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf)
  {

    this->world = _parent;
    this->sdf = _sdf;

    // Load the spatial reference system (simulation coordinates)
    gzdbg << "try to get projection" << std::endl;
    this->sdf->PrintValues("dfs");
    this->srs.importFromEPSG(this->sdf->GetElement("projection")->Get<int>("epsg"));
    gzdbg << "fot it. to get projection" << std::endl;
    
    // Load the currents model parameters
    sdf::ElementPtr oceanSDF = this->sdf->GetElement("ocean_effects");
    if ( oceanSDF->HasElement("currents") )
      {
	std::string model_type = oceanSDF->GetElement("currents")->Get<std::string>("model");
	if ( model_type == "fvcom" || model_type == "FVCOM" )
	  {
	    this->ocean_effects.currents.model = ocean_currents_model_t::FVCOM;

	    // Read parameters for model to use.
	    sdf::ElementPtr fvcomSDF = oceanSDF->GetElement("currents")->GetElement("fvcom");	    
	    this->ocean_effects.currents.fvcom_uri = fvcomSDF->Get<std::string>("uri");
	    this->ocean_effects.currents.fvcom_srs.importFromEPSG(fvcomSDF->GetElement("projection")->Get<int>("epsg"));
	  }
	// @@@ other models and default to constant 0,0.
      }
    
    // Listen to the update event.
    this->updateConnection = event::Events::ConnectWorldUpdateBegin(boost::bind(&WindHydroPlugin::OnUpdate, this, _1));

  }
  public: void OnUpdate(const common::UpdateInfo& _info)
  {
    
    // Walk though all active models and compare against existing list.
    // Add any new ones, delete any that have disappeared.
    for (auto& ii : this->world->GetModels())
      {
	bool isnew =  true;
	for ( auto& iii : this->models )
	  {
	    if ( ii->GetId() == std::get<0>(iii)->GetId() )
	      {
		isnew = false;
	      }
	  }
	if ( isnew ) // in the list of active models but not in the internal list
	  {

	    this->models.push_back(std::make_tuple(ii,static_cast<ocean_models::ModelInterface*>(nullptr))); // @@@ add the other physics models here.

	    // @@@ this is where we traverse the SDF! for <hydrodynamics>

	    // don't want to maintain a list of all possible models, because many of them will not care.

	    if (ii->GetSDF()->HasElement("plugin"))
	      {
		sdf::ElementPtr pluginSDF = ii->GetSDF()->GetElement("plugin");
		while (pluginSDF) 
		  {
		    if (pluginSDF->HasElement("ocean_effects"))
		      {
			sdf::ElementPtr oceanSDF = pluginSDF->GetElement("ocean_effects");
			if ( oceanSDF->HasElement("currents") && oceanSDF->Get<bool>("currents"))
			  {

			    if ( this->ocean_effects.currents.model == ocean_currents_model_t::FVCOM)
			      {

				gzdbg << "Loading ocean model FVCOM for " << ii->GetName() << "..." << std::endl;
				ocean_models::ModelInterface* ocean_model = new ocean_models::FVCOM(this->ocean_effects.currents.fvcom_uri);

				this->models.pop_back();
				std::tuple<gazebo::physics::ModelPtr,ocean_models::ModelInterface*> replacement(ii,ocean_model);
				this->models.push_back(replacement);
				  
				gzdbg << "Loading ocean model FVCOM for " << ii->GetName() << "... Success." << std::endl;

				// Epoch conversion: 1970-01-01 00:00:00 is Modified Julian Day 40587
				double jtime = 40587.0*86400.0 + _info.simTime.Double();
				
				// Initialize the cache.  Need to convert between SRSs.
				this->ocean_effects.currents.fvcom_poCT
				  = OGRCreateCoordinateTransformation(&this->srs, &this->ocean_effects.currents.fvcom_srs );
				gazebo::math::Pose pose = ii->GetWorldPose();				
				double sNorthing = pose.pos.y; // native projection (source coordinates)
				double sEasting = pose.pos.x;
				double tNorthing = sNorthing; // This is both the input and output to Transform() below.
				double tEasting = sEasting;
				if( this->ocean_effects.currents.fvcom_poCT == NULL
				    || !this->ocean_effects.currents.fvcom_poCT->Transform( 1, &tEasting, &tNorthing ) )
				  {
				    gzerr << "Projected coordinate transformation failed." << std::endl;
				  }
				else
				  {

				    ocean_models::ModelData ocean_data = ocean_model->getData(
											      tEasting, // easting 
											      tNorthing, // northing
											      pose.pos.z, // up
											      jtime); // time, modified Julian seconds
				    std::cout << "got currents east:" << ocean_data.u
					      << " north:" << ocean_data.v << std::endl;
				  }
			      }
			    else
			      {
				gzerr << "Unsupported ocean currents model type." << std::endl;
				// @@@ this should not happen if we default to constant during the Load() phase.
			      }
			  }
		      }
		    pluginSDF = pluginSDF->GetNextElement();
		  }
	      }
	  }
      }

    // @@@ presently deleting lrauv crashes gazebo.  But ceiling plane is ok to remove, so not sure why lrauv would be special other than that it is last in the list?  gazebo crashes even with this commented out.  I don't think it's the issue.
    auto ii = this->models.begin();
    while (ii != this->models.end())
      {
	bool isgone = true;
	for ( auto& iii : this->world->GetModels() )
	  {
	    if ( std::get<0>(*ii)->GetId() == iii->GetId() )
	      {
		isgone = false;
	      }
	  }
	if ( isgone ) // in the internal list but not in the list of active models
	  {
	    ii = this->models.erase(ii);
	  }
	else
	  {
	    ++ii;
	  }
      }
    // @@@ modifying models from the list from this point on introduces a race condition doesn't it?  What if
    // model gets deleted between now and operating on it.  Depends on how plugins work.  Are their update
    // functions atomic?
    // @@@ we do this above so that we can maintain the ocean_model caches for each model.
    // @@@ the dynamics maybe could be simplified by some kind of dynamic creation of F=ma that wouldn't
    // @@@ require traversing the SDF for each model every update.  right - you load the parameters for the
    // @@@ hydro function.


    // For each model compute the hydrodynamic forces using the scheme
    // specified in the model's SDF.
    // Only a few hydrodynamic derivatives supported for now.
    // Traverse the model's SDF to find links with the <hydrodynamics> tag.
    // @@@ move this to when new models are found only.
    for ( auto& mm : this->models )
      {
	gazebo::physics::ModelPtr ii = std::get<0>(mm);
	if (ii->GetSDF()->HasElement("plugin"))
	  {
	    sdf::ElementPtr pluginSDF = ii->GetSDF()->GetElement("plugin");
	    while (pluginSDF)  // traverse plugins
	      {
		if (pluginSDF->HasElement("hydrodynamics"))
		  {
		    sdf::ElementPtr hydroSDF = pluginSDF->GetElement("hydrodynamics");


		    // @@@ need to buffer currents and add an option to reduce update rate.
		    
		    // Get currents.  All models need this.
		    double U = 0.0;
		    double V = 0.0;

		    // Epoch conversion: 1970-01-01 00:00:00 is Modified Julian Day 40587
		    double jtime = 40587.0*86400.0 + _info.simTime.Double();
		    
		    // @@@ do conversions/srs
		    gazebo::math::Pose pose = ii->GetWorldPose();				
		    double sNorthing = pose.pos.y; // native projection (source coordinates)
		    double sEasting = pose.pos.x;
		    double tNorthing = sNorthing; // This is both the input and output to Transform() below.
		    double tEasting = sEasting;
		    if( this->ocean_effects.currents.fvcom_poCT == NULL
			|| !this->ocean_effects.currents.fvcom_poCT->Transform( 1, &tEasting, &tNorthing ) )
		      {
			gzerr << "Projected coordinate transformation failed." << std::endl;
		      }
		    else
		      {
			std::cout.precision(17);
			//gzdbg << "easting" << tEasting << " northing:" << tNorthing << " jtime:" << std::fixed << jtime << std::endl;
			ocean_models::ModelInterface* ocean_model = std::get<1>(mm);
			try
			  {
			    ocean_models::ModelData ocean_data = ocean_model->getData(
										      tEasting, // easting 
										      tNorthing, // northing
										      pose.pos.z, // up
										      jtime); // time, modified Julian seconds
			    U = ocean_data.v; // reversed convention
			    V = ocean_data.u;
			    //gzdbg << "Currents for " << ii->GetName() << " U:" << U << " V:" << V << std::endl;
			  }
			catch(const std::out_of_range& e)
			  {
			    gzwarn << "currents out of range" << std::endl; // probably too deep.
			    U = 0.0;
			    V = 0.0;
			  }
	
		      }

		    // All models need certain physical constants
		    double g = ii->GetWorld()->GetPhysicsEngine()->GetGravity()[2];

		    // @@@@ need to move this into the model initialzation bit.
		    // @@@ also need to try multiple LRAUV.
		    while (hydroSDF)  // traverse <hydrodynamics> tags.
		      {
			
			// Apply a hydrodynamics model according to the type specified
			if (!hydroSDF->HasAttribute("method") )
			  {
			    gzerr << "<hydrodynamics> tag must have the method attribute, i.e. method=" << std::endl;
			  }
			else
			  {
			    if (hydroSDF->Get<std::string>("method") == "body_derivatives")
			      {
				gazebo::physics::LinkPtr body_link = ii->GetLink(hydroSDF->Get<std::string>("reference"));
				
				// @@@@ move this into a separate function of course.  and make robust wrt missing derivatives.

				double m = body_link->GetInertial()->GetMass();
				
				double Xuu = hydroSDF->Get<double>("Xuu");
				double Yv = hydroSDF->Get<double>("Yv");
				double Zw = hydroSDF->Get<double>("Zw");
				double Kp = hydroSDF->Get<double>("Kp");
				double Mq = hydroSDF->Get<double>("Mq");
				double Nr = hydroSDF->Get<double>("Nr");
				double zGB = hydroSDF->Get<double>("zGB");
				
				// Gazebo body-frame velocities and rates: Fwd-Port-Up
				math::Vector3 vel_lin = body_link->GetRelativeLinearVel();
				math::Vector3 vel_ang = body_link->GetRelativeAngularVel();

				// Convert to SNAME notation
				double u = vel_lin[0];
				double v = -vel_lin[1];
				double w = -vel_lin[2];
				double p = vel_ang[0];
				double q = -vel_ang[1];
				double r = -vel_ang[2];
				
				// Gazebo pose
				math::Pose pose = body_link->GetWorldPose();
				
				// Convert to SNAME notation
				// @@@ roll, pitch yaw are a different convention ("fixed axis"); not euler angles
				double phi = pose.rot.GetRoll();
				double theta = -pose.rot.GetPitch();
				double psi = M_PI/2.0-pose.rot.GetYaw();
				
				// compute relative currents
				double ur = u - (cos(psi)*U + sin(psi)*V);  // @@@ 2D only.
				double vr = v - (-sin(psi)*U + cos(psi)*V);
				double wr = w;
				
				// model here.
				double X = Xuu*ur*fabs(ur);
				double Y = Yv*vr;
				double Z = Zw*wr;
				double K = Kp*p + zGB*sin(phi)*m*g;
				double M = Mq*q + zGB*sin(theta)*m*g;
				double N = Nr*r;
				
				// Apply forces to body link.
				math::Vector3 force = {X,-Y,-Z};
				math::Vector3 torque = {K,-M,-N};
				body_link->AddRelativeForce(force);
				body_link->AddRelativeTorque(torque);
			    
			      }
			    else if (hydroSDF->Get<std::string>("method") == "fin_to_body_derivatives")
			      {
				gazebo::physics::LinkPtr body_link = ii->GetLink(hydroSDF->Get<std::string>("reference_body"));
				gazebo::physics::JointPtr joint = ii->GetJoint(hydroSDF->Get<std::string>("reference_joint"));
				
				double Kuudf = hydroSDF->Get<double>("Kuudf");
				double Muudf = hydroSDF->Get<double>("Muudf");
				double Nuudf = hydroSDF->Get<double>("Nuudf");
				//double Nuur = hydroSDF->Get<double>("Nuur"); // this may belong more in the body hydro.
				
				// Gazebo body-frame velocities and rates: Fwd-Port-Up
				math::Vector3 vel_lin = body_link->GetRelativeLinearVel();
				math::Vector3 vel_ang = body_link->GetRelativeAngularVel();
				
				// Convert to SNAME notation
				double u = vel_lin[0];
				double v = -vel_lin[1];
				double w = -vel_lin[2];
				double p = vel_ang[0];
				double q = -vel_ang[1];
				double r = -vel_ang[2];
				
				// Gazebo pose
				math::Pose pose = body_link->GetWorldPose();
				
				// Convert to SNAME notation
				// @@@ roll, pitch yaw are a different convention ("fixed axis"); not euler angles
				double phi = pose.rot.GetRoll();
				double theta = -pose.rot.GetPitch();
				double psi = M_PI/2.0-pose.rot.GetYaw();
				
				// compute relative currents
				double ur = u - (cos(psi)*U + sin(psi)*V);  // @@@ 2D only.
				double vr = v - (-sin(psi)*U + cos(psi)*V);
				double wr = w;
				
				// get fin angle
				double fin_angle = joint->GetAngle(0).Radian();
				
				// Compute body forces.
				double K = Kuudf*ur*fabs(ur)*fin_angle; // small angles
				double M = Muudf*ur*fabs(ur)*fin_angle;
				double N = Nuudf*ur*fabs(ur)*fin_angle;
				
				// Apply forces to body.
				math::Vector3 torque = {K,-M,-N};
				body_link->AddRelativeTorque(torque);
				
			      }
			    else if (hydroSDF->Get<std::string>("method") == "propeller_to_body_derivatives")
			      {
				gazebo::physics::LinkPtr body_link = ii->GetLink(hydroSDF->Get<std::string>("reference_body"));
				gazebo::physics::JointPtr shaft = ii->GetJoint(hydroSDF->Get<std::string>("reference_joint"));


				
				double Xrpm = hydroSDF->Get<double>("Xrpm");
				// could also apply a simple advance-ratio dependent propeller model.

				double shaft_speed_rpm = shaft->GetVelocity(0)/M_PI/2.0*60.0;

				// Compute forces to apply to main body.
				double X = Xrpm*shaft_speed_rpm;
				double Y = 0.0;
				double Z = 0.0;

				// Apply forces to body.
				math::Vector3 force = {X,-Y,-Z};
				body_link->AddRelativeForce(force);
				
			      }
			    else
			      {
				gzerr << "<hydrodynamics> tag for " << ii->GetName() << "specifies an unsupported method." << std::endl;
			      }
			  }
			hydroSDF = hydroSDF->GetNextElement("hydrodynamics");
		      }
		  }
		pluginSDF = pluginSDF->GetNextElement("plugin");
	      }
	  }
      }
  }

  
  private: gazebo::physics::WorldPtr world;
  private: sdf::ElementPtr sdf;
  private: OGRSpatialReference srs; // spatial reference system

  private: ocean_effects_t ocean_effects;
  
  protected: gazebo::event::ConnectionPtr updateConnection;

  // @@@ need to templatize this or something to allow for mulple model types.
  // @@@ will some models have hydrodynamics but not currents?  perhaps, but storing a pointer is no big deal.
  private: std::list< std::tuple<gazebo::physics::ModelPtr,ocean_models::ModelInterface*> > models;
  
};

// Register this plugin with the simulator
GZ_REGISTER_WORLD_PLUGIN(WindHydroPlugin)
}
