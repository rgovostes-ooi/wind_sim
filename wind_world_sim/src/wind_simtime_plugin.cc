
#include "gazebo/physics/physics.hh"
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"

#include <ros/ros.h>

bool loadString(std::string &result, const sdf::ElementPtr& _sdf, const std::string& tag) {
    if (!_sdf->HasElement(tag)) {
        ROS_FATAL_STREAM("ROS plugin wind_world requires a tag \"" <<tag <<"\" but it wasn't found!");
        return false;
    }

    result = _sdf->Get<std::string>(tag);

    // remove any double-quotes
    result.erase(std::remove(result.begin(), result.end(), '\"'),result.end());

    ROS_INFO_STREAM("ROS plugin wind_world load \"" <<tag <<"\" = \"" <<result <<"\"");

    return true;
}

bool loadDouble(double& result, const sdf::ElementPtr& _sdf, const std::string& tag) {
    if (!_sdf->HasElement(tag)) {
        ROS_FATAL_STREAM("ROS plugin wind_world requires a tag \"" <<tag <<"\" but it wasn't found!");
        return false;
    }

    result = _sdf->Get<double>(tag);

    ROS_INFO_STREAM("ROS plugin wind_world load \"" <<tag <<"\" = \"" <<result <<"\"");

    return true;
}

bool loadInt(int& result, const sdf::ElementPtr& _sdf, const std::string& tag) {
    if (!_sdf->HasElement(tag)) {
        ROS_FATAL_STREAM("ROS plugin wind_world requires a tag \"" <<tag <<"\" but it wasn't found!");
        return false;
    }

    result = _sdf->Get<int>(tag);

    ROS_INFO_STREAM("ROS plugin wind_world load \"" <<tag <<"\" = \"" <<result <<"\"");

    return true;
}



namespace gazebo
{
class WindSimTimePlugin : public WorldPlugin
{
  public: void Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf)
  {

    this->world = _parent;
    this->sdf = _sdf;
    
    // Set simulation time according to launch file.
    double sim_start_time;
    if ( loadDouble(sim_start_time, this->sdf, "sim_start_time") )
      {
	gazebo::common::Time gzt(sim_start_time,0);
	this->world->SetSimTime(gzt);
      }
    // @@@ else set to the start of the ocean file?

  }

private: gazebo::physics::WorldPtr world;
private: sdf::ElementPtr sdf;
  
};

// Register this plugin with the simulator
GZ_REGISTER_WORLD_PLUGIN(WindSimTimePlugin)
}
